# Contributor: Holger Jaekel <holger.jaekel@gmx.de>
# Maintainer: psykose <alice@ayaya.dev>
pkgname=aws-c-io
pkgver=0.13.15
pkgrel=0
pkgdesc="Module for the AWS SDK for C handling all IO and TLS work for application protocols"
url="https://github.com/awslabs/aws-c-io"
# s390x: aws-c-common
arch="all !s390x"
license="Apache-2.0"
makedepends="
	aws-c-cal-dev
	aws-c-common-dev
	cmake
	openssl-dev
	s2n-tls-dev
	samurai
	"
subpackages="$pkgname-dev"
source="$pkgname-$pkgver.tar.gz::https://github.com/awslabs/aws-c-io/archive/refs/tags/v$pkgver.tar.gz"
options="net" # required for tests to make connections

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	CFLAGS="$CFLAGS -flto=auto" \
	CXXFLAGS="$CXXFLAGS -flto=auto" \
	cmake -B build -G Ninja \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=/usr/lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=None \
		-DBUILD_TESTING="$(want_check && echo ON || echo OFF)" \
		$CMAKE_CROSSOPTS
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure -j${JOBS:-2}
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

dev() {
	default_dev
	amove usr/lib/aws-c-io
}

sha512sums="
2bfc8b69bfb852f77f3d8658adb3e7d5bb83c83c3f8d97f527af5e8bd11778c482dc4dc24ce6220953806c36c3e1bb6359f26ca05536e5d63057855d57fee33d  aws-c-io-0.13.15.tar.gz
"
