# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=breeze-gtk
pkgver=5.27.0
pkgrel=0
pkgdesc="A GTK Theme Built to Match KDE's Breeze"
# armhf blocked by extra-cmake-modules
arch="noarch !armhf"
url="https://kde.org/plasma-desktop/"
license="LGPL-2.1-only"
depends="gtk-engines"
makedepends="
	breeze
	breeze-dev
	extra-cmake-modules
	py3-cairo
	samurai
	sassc
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/breeze-gtk-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
8ed6ff0140d817d301f1f0b1551261fe7db8947584e6429d82d4b12cc64432837f50eb96f08075279efa2c37e812835705e78b59250cf86dac9900061dd1b26a  breeze-gtk-5.27.0.tar.xz
"
