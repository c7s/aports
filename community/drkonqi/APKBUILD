# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=drkonqi
pkgver=5.27.0
pkgrel=0
pkgdesc="The KDE crash handler"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/plasma-desktop/"
license="GPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	kcompletion-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kcrash-dev
	kdeclarative-dev
	ki18n-dev
	kidletime-dev
	kio-dev
	kjobwidgets-dev
	knotifications-dev
	kservice-dev
	kuserfeedback-dev
	kwallet-dev
	kwidgetsaddons-dev
	kxmlrpcclient-dev
	qt5-qtbase-dev
	qt5-qtx11extras-dev
	samurai
	syntax-highlighting-dev
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/drkonqi-$pkgver.tar.xz"
subpackages="$pkgname-lang"
options="!check" # Broken

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_TESTING=OFF # Broken
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
4f89e1b6671ed3597ed19d399a65c812ee8a0e96ed7889f832bd489b29a1feb431954b5f2fe31af1cfd5bf18f89d1b5dc9c102f8613b6e4b096892d833f4625f  drkonqi-5.27.0.tar.xz
"
