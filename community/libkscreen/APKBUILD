# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=libkscreen
pkgver=5.27.0
pkgrel=0
pkgdesc="KDE screen management software"
arch="all !armhf" # armhf blocked by extra-cmake-modules
url="https://kde.org/plasma-desktop/"
license="LGPL-2.1-or-later AND GPL-2.0-or-later AND (GPL-2.0-only OR GPL-3.0-only)"
depends_dev="
	kconfig-dev
	kwayland-dev
	qt5-qtbase-dev
	qt5-qtx11extras-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	graphviz
	plasma-wayland-protocols
	qt5-qttools-dev
	samurai
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/libkscreen-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc"
options="!check" # Fails due to requiring dbus-x11 and it running

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build

	rm -rf "$pkgdir"/usr/lib/systemd
}

sha512sums="
b568d34a5cc8adb328852d8c41f8d61ca21d6c860d3c72b62574c71a01427eec1aedabe2f445f391441dc741620684bde50a5d9ba58d16604594a2a249dcd0a7  libkscreen-5.27.0.tar.xz
"
