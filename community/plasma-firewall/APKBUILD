# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=plasma-firewall
pkgver=5.27.0
pkgrel=0
pkgdesc="Control Panel for your system firewall"
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
url="https://kde.org/plasma-desktop/"
license="GPL-2.0-or-later"
depends="
	python3
	kirigami2
	"
makedepends="
	extra-cmake-modules
	kcmutils-dev
	kcoreaddons-dev
	kdeclarative-dev
	ki18n-dev
	plasma-framework-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtx11extras-dev
	samurai
	"
checkdepends="xvfb-run"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/plasma-firewall-$pkgver.tar.xz"
subpackages="$pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
fa493ed2ea25bd94731771506f9041ac2f67ab91f92516122d837da228ca56e6a4fb1c6043b5a2e8155962d6dd1f777f46623733d5d551d7712670eb6914532c  plasma-firewall-5.27.0.tar.xz
"
